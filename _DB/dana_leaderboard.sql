-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 20 Bulan Mei 2019 pada 02.28
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dana_leaderboard`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `desc` text,
  `status` int(11) DEFAULT NULL,
  `date_create` int(11) DEFAULT NULL,
  `date_edit` varchar(100) DEFAULT NULL,
  `create_by` int(2) DEFAULT NULL,
  `edit_by` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `campaign`
--

INSERT INTO `campaign` (`id`, `nama`, `desc`, `status`, `date_create`, `date_edit`, `create_by`, `edit_by`) VALUES
(6, 'quiz number 1', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'quiz number 2a', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'quiz number 555', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'qyiz asdfasdfas', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_user`
--

CREATE TABLE `data_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `sosmed` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0',
  `campaign_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_user`
--

INSERT INTO `data_user` (`id`, `nama`, `sosmed`, `score`, `status`, `campaign_id`) VALUES
(2, 'asdfas', 'asdfasdfa', '1111', '0', 9),
(3, 'safdasd', 'sadfasfa', '12390', '0', 9),
(4, 'sadfasdf', 'sadfsadfa', '111', '0', 9),
(5, 'sadfasd', '@dsafasd', '90999', '0', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `page_leaderboard`
--

CREATE TABLE `page_leaderboard` (
  `id` int(11) UNSIGNED NOT NULL,
  `leader1` int(11) DEFAULT NULL,
  `leader2` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `img_logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `page_leaderboard`
--

INSERT INTO `page_leaderboard` (`id`, `leader1`, `leader2`, `img`, `img_logo`) VALUES
(1, 9, 0, 'bg-1558067647promothumb-1554889235DANA-(1).png', 'logo-1558015231logo.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT 'tes',
  `level` varchar(11) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `last_login` varchar(33) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `email`, `status`, `last_login`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', 'admin', 0, ''),
(131, 'dani', 'e94d51a35484755a9f9672d13687f499', '0', 'me@danirus.com', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_user`
--
ALTER TABLE `data_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `page_leaderboard`
--
ALTER TABLE `page_leaderboard`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `data_user`
--
ALTER TABLE `data_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `page_leaderboard`
--
ALTER TABLE `page_leaderboard`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
