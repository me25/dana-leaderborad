<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dana extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');
 
	}
	public function index()
	{
		$list_query= $this->Modglobal->find('page_leaderboard', array('id' => '1'));
		$page_detail = $list_query->row_array();

		//echo $page_detail['id'];

		$data = array(
			'content' => 'dana/index',
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/kosong', $data);
	}
}


