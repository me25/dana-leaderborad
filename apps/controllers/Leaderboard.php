<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leaderboard extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Rule";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('campaign', array());
		$campaign = $query->result_array();

		$query_leaderboard= $this->Modglobal->find('page_leaderboard', array('id' => '1'));
		$page_detail = $query_leaderboard->row_array();

		$data = array(
			'content' => 'leaderboard/form',
			'campaign' => $campaign,
			'page_detail' => $page_detail,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'bg-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = 'media/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

	    if($_FILES["img_logo"]['name']) {
			$img_logo = 'logo-'.time().$_FILES["img_logo"]['name'];
			$img_logo = str_replace($findArr, $replaceArr, $img_logo);
			$config['upload_path']   = 'media/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_logo;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_logo');
	    }
	    else{
	    	$img_logo = $this->input->post('img_logo2');
	    }
	    
		$data = array(
        	'leader1' => $this->input->post('leader1'),
        	'leader2' => $this->input->post('leader2'),
        	'img' => $img,
        	'img_logo' => $img_logo,
        );
    	$where = array(
    		'id' => '1',
        );
        $this->Modglobal->update('page_leaderboard', $data, $where);
		redirect('leaderboard');
	}
}


