<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');
 
	}
	public function index()
	{
		if($this->session->userdata('logged_in')){
			redirect(base_url("home"));
		}
		else{
			$this->load->view('auth/index');
		}
	}
	public function process(){
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$continue = $this->input->post('continue');

		$checkemail = $this->Modglobal->find('user', array('email' => $email));
		$checkemail_num = $checkemail->num_rows();

		if($checkemail_num > 0) {
			$check_user = $this->Modglobal->find('user', array('email' => $email, 'password' => $password));
			$cekuser = $check_user->num_rows();
			if($cekuser > 0) {
				$check_user_aktif = $this->Modglobal->find('user', array('email' => $email, 'password' => $password, 'status' => 0));
				$cekuser_aktif = $check_user_aktif->num_rows();
				if($cekuser_aktif > 0) {
					$user = $check_user->row();
					$session_data = array(
						'id' => $user->id,
						'username' => $user->username,
						'email' => $user->email,
						'level' => $user->level,
						'logged_in' => 1
					);
					$this->session->sess_expiration = '172800';
					$this->session->set_userdata($session_data);
					if($continue!==""){
						redirect($continue);
						//redirect($_GET['continue']);
						//redirect(base_url("home")'/'.$_GET['continue']);
					}
					else{
						redirect(base_url("home"));
					}
				}
				else{
					$this->session->set_flashdata('error', 'Akun belum di aktivasi, cek email untuk mengaktivasi');
					redirect(base_url("auth"));
				}
				
			}
			else{
				$this->session->set_flashdata('error', 'Password Salah');
				redirect(base_url("auth"));
			}

		}
		else{
			$this->session->set_flashdata('error', 'Email tidak terdaftar');
			redirect(base_url("auth"));
		}

		
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('auth'));

	}
	
}


