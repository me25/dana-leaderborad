<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><h2>Change Password</h2></div>
			<div class="clearfix"></div>
		</div>
		
		<hr color="#eee">
		<form action="<?php echo base_url('home/password_update');?>" class="form_1" method="post">
			<?php 
			if ($this->session->flashdata('success')) { 
				echo'<div class="notif_bar">
				  Kata sandi berhasil diperbaharui
				</div>';
			}
			elseif ($this->session->flashdata('verify_fail')) {
				echo'<div class="notif_bar notif_bar_fail">
				  Kata sandi gagal diperbaharui, cek kembali konfirmasi kata kunci baru Anda
				</div>';
			}
			elseif ($this->session->flashdata('fail')) {
				echo'<div class="notif_bar notif_bar_fail">
				  Kata sandi gagal diperbaharui, cek kembali kata kunci lama Anda
				</div>';
			}
			else{}
			?>
		    <div class="form-group  form-group-col-2">
		      	<strong>Current Password</strong>
		      	<input type="password" name="password_current" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group  form-group-col-2">
		      	<strong>New Password</strong>
		      	<input type="password" name="password_new" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group  form-group-col-2">
		      	<strong>Verify Password</strong>
		      	<input type="password" name="password_verify" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <br>
		    <div>
		    	<a href="<?php echo base_url('member');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>