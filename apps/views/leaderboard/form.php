
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><h2>Leaderboard Page Editor</h2></div>
			<div class="clearfix"></div>
		</div>

		<hr color="#eee">
		<form action="<?php echo base_url('leaderboard/update');?>" class="form_1 form_posisi" method="post"  enctype="multipart/form-data">
			<div class="form-group">
		      	<strong>Logo Image</strong>
		      	<input type="file" name="img_logo" id="uploadFile" class="uploadFile">
		      	<input type="hidden" name="img_logo2" value="<?php echo $page_detail['img_logo'];?>">
		      	<h6>Image Size max:600x100px, max file size 500kb</h6>
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img_logo']){
		      				echo '<img src="media/'.$page_detail['img_logo'].'" alt="" height="80">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="form-group">
		      	<strong>Background Image</strong>
		      	<input type="file" name="img" id="uploadFile" class="uploadFile">
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<h6>Image Size max:1440x800px (rasio 16:9), max file size 1mb</h6>
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="media/'.$page_detail['img'].'" alt="" height="80">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Leaderboard 1</strong>
		      	<div class="select-style">
					<span></span>
					<select name="leader1" id="">
						<?php
						echo '<option value="0">Kosong</option>';
						foreach ($campaign as $row) {
							if($page_detail['leader1'] == $row['id']){
								echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
							}
							else{
								echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
							}
						}
					?>
					</select>
					
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="form-group">
		      	<strong>Leaderboard 2</strong>
		      	<div class="select-style">
					<span></span>
					<select name="leader2" id="">
						<?php
						echo '<option value="0">Kosong</option>';
						foreach ($campaign as $row) {
							if($page_detail['leader2'] == $row['id']){
								echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
							}
							else{
								echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
							}
						}
					?>
					</select>
					
				</div>
		      	<div class="clearfix"></div>
		    </div>
			<br>
			<div>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $(".uploadFile").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	    $(".uploadFile2").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview2');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/><img src="assets/images/ico_delete.png" alt="" class="hapus">');
	                $(".hapus").click(function() {
				    	$(this).parent().parent().find('.img_show').val("");
				    	$(this).parent().parent().find('.uploadFile2').val("");
				    	$(this).parent().parent().find('.imagePreview2').html("");
				    	
				    	//alert(isi);
				    });
	            }
	        }
	    });

	    $(".hapus").click(function() {
	    	$(this).parent().parent().find('.img_show').val("");
	    	$(this).parent().parent().find('.imagePreview').html("");
	    	
	    	//alert(isi);
	    });
	    

	});
</script>




