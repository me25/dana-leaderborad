<html>
<head>
	<head>
	<?php //error_reporting(0);?>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="This is an example of a meta description. This will often show up in search results.">
	<meta name="msapplication-TileColor" content="#0d2b7d">

	<title>DANA CMS</title>
	<link href="<?php echo assets_url('css');?>/preview.css?1" rel="stylesheet" type="text/css">
	</head>

</head>
<body>
	<div class="body_front" style="background-image: url('<?php echo base_url('media/').$page_detail['img'];?>')">
		<div class="logo">
			<img src="<?php echo base_url('media/').$page_detail['img_logo'];?>" alt="">
		</div>
		<div class="clearfix"></div>
		<div class="area">
			<?php	
				$leader1_query= $this->Modglobal->find('campaign', array('id' => $page_detail['leader1']));
				$leader1 = $leader1_query->row_array();

				$leader2_query= $this->Modglobal->find('campaign', array('id' => $page_detail['leader2']));
				$leader2 = $leader2_query->row_array();
				

			?>
			<div class="box">
				<div class="title"><?php echo $leader1['nama'];?></div>
				<div class="list">
					<ul>
						<?php
							$user_leader1_query= $this->Modglobal->find('data_user', array('campaign_id' => $leader1['id']),'score desc','','3','0');
							$user = $user_leader1_query->result_array();

							//echo $page_detail['leader1'];

							foreach ($user as $row) {
								echo'<li>
									<div class="name">'.$row['nama'].'</div>
									<div class="id">'.$row['sosmed'].'</div>
									<div class="score">'.$row['score'].'</div>
									<div class="clearfix"></div>
								</li>';
							}
						?>
					</ul>
				</div>
			</div>
			<?php
				if($page_detail['leader2']!=="0"){
					$user_leader2_query= $this->Modglobal->find('data_user', array('campaign_id' => $leader2['id']),'score desc','','3','0');
					$user2 = $user_leader2_query->result_array();
					?>
					<div class="box">
						<div class="title"><?php echo $leader2['nama'];?></div>
						<div class="list">
							<ul>
								<?php
									

									//echo $page_detail['leader1'];

									foreach ($user2 as $row) {
										echo'<li>
											<div class="name">'.$row['nama'].'</div>
											<div class="id">'.$row['sosmed'].'</div>
											<div class="score">'.$row['score'].'</div>
											<div class="clearfix"></div>
										</li>';
									}
								?>
							</ul>
						</div>
					</div>
					<?php

				}
				else{}
				?>

		</div>
	</div>

	
</body>
</html>