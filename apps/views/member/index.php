
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><?php echo $member_num;?> Admin Member</div>
			<a href="<?php echo base_url('member/add');?>" class="btn_add">+ Tambah</a>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		            <th>Name</th>
		            <th>Email</th>
		            <!-- <th>Role</th> -->
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($member as $row_member) { 
		    		?>
		    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td><?php echo $row_member['username'];?></td>
			            <td><?php echo $row_member['email'];?></td>
			            <!-- <td><?php echo $level[$row_member['level']];?></td> -->
			            <td class="action">
			            	<a href="<?php echo base_url('member/edit/').$row_member['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('member/delete/').$row_member['id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>