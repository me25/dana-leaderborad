
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><?php echo $member_num;?> Daftar Peserta</div>
			<a href="<?php echo base_url('campaign/add_peserta/').$page_detail['id'];?>" class="btn_add">+ Tambah</a>
			<!-- <form action="<?php echo base_url('campaign/importcsv/');?>" class="import" method="post" enctype="multipart/form-data">
				<span>Import CSV</span>
				<input type="file" name="file">
				<input name="campaign_id" type="hidden" value="<?php echo $page_detail['id'];?>">
				<input type="submit" value="Upload" class="btn_add">
			</form> -->
			
			<div class="clearfix"></div>
		</div>

		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		        	<th>Score</th>
		            <th>Name</th>
		            <th>Social Media</th>
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($list as $row) { 
		    		?>
		    		<tr>
		    			<td><?php echo $row['score'];?></td>
			            <td><?php echo $row['nama'];?></td>
			            <td><?php echo $row['sosmed'];?></td>
			            <td class="action">
			            	<a href="<?php echo base_url('campaign/edit_peserta/').$row['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('campaign/delete_peserta/').$row['id'].'/'.$row['campaign_id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>