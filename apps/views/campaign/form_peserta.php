
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = 'Add '.$page;
				$action = 'campaign/addpeserta_pro';
			}
			else{
				$title = 'Edit '.$page;
				$action = 'campaign/updatepeserta';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"  enctype="multipart/form-data">
			<div class="form-group">
		      	<strong>Name</strong>
		      	<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
		      	<input type="hidden" name="campaign_id" value="<?php echo $id_campaign;?><?php echo $page_detail['campaign_id'];?>">
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <div class="form-group">
		      	<strong>Social Media</strong>
		      	<input type="text" name="sosmed" value="<?php echo $page_detail['sosmed'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>


		    <div class="form-group">
		      	<strong>Score</strong>
		      	<input type="number" name="score" value="<?php echo $page_detail['score'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		   
		    <div class="clearfix"></div>
		    
		    <br>
		    <div>
		    	<a href="<?php echo base_url('campaign/peserta/').$id_campaign;?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
</script>