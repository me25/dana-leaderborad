<header>
	<a class="logo">
		<img src="<?php echo assets_url('images');?>/logo_dana.png" alt="">
	</a>
	<nav>
		<a href="<?php echo base_url('home');?>" class="<?php if($page=="dashboard") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_dashboard.png" alt="">
			</span>
			<span class="menu">Dashboard</span>
		</a>
		<a href="<?php echo base_url('campaign');?>" class="<?php if($page=="Campaign") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Campaign</span>
		</a>
		<a href="<?php echo base_url('leaderboard');?>" class="<?php if($page=="Leaderboard") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Leaderboard Page</span>
		</a>
		<a href="<?php echo base_url('dana');?>" target="_blank">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Run Page</span>
		</a>
		

		<a href="<?php echo base_url('member');?>" class="<?php if($page=="member") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Admin</span>
		</a>
	</nav>
</header>
